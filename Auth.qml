import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtQuick 2.0
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1

Page{
    id: page8Form
    Material.background: "#fffff"

    header:   Rectangle {


        Connections {
            target: httpController // Указываем целевой объект для соединения

            onTokenToQml: {
                tokenShow.text = token // Устанавливаем аву
            }

            onDataToQml: {
                base64data.text = pageContent;
            }
        }

        AnimatedImage {
            id: animatedImage
            x: 8
            y: 8
            width: 75
            height: 75
            source: "politech.png"
        }

        Label {
            anchors.left: animatedImage.right
            anchors.leftMargin: 20
            anchors.horizontalCenter: header.horizontalCenter
            anchors.verticalCenter: header.verticalCenter
            x: 16
            y: 0
            text: qsTr("АВТОРИЗАЦИЯ")
            font.pixelSize: 30
            styleColor: "#000000"
            padding: 10

        }
        id: rectangleHeader
        x: 0
        y: 0
        width: 600
        height: 95
        anchors.right: parent.right
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#1262cb"
            }

            GradientStop {
                position: 0.98157
                color: "#1262cb"
            }



        }
        anchors.left: parent.left
    }

    GridLayout {
        id: mainLayout
        columns: 2

        rowSpacing: 5
        columnSpacing: 5

        anchors {
            top: parent.top;
            left: parent.left
            right: parent.right
        }

        Label {
            id: loginLabel

            anchors.left: parent.left
            anchors.leftMargin: 15
            font.pixelSize: 24
            text: "Логин"
        }

        TextField {
            id: login

            anchors.left: loginLabel.right
            anchors.leftMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
            Material.accent: "#1262cb"
            font.pixelSize: 20
        }

        Label {
            id: passwordLabel
            font.pixelSize: 24
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.topMargin:  60
            anchors.top: login.bottom

            text: "Пароль"
        }

        TextField {
            id: password

            anchors.left: passwordLabel.right
            anchors.leftMargin: 20
            anchors.topMargin:  60
            anchors.top: login.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            Material.accent: "#1262cb"
            font.pixelSize: 20
        }

        Button{
            id: send

            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.right:  parent.right
            anchors.rightMargin:  15
            anchors.top: password.bottom
            anchors.topMargin: 30
            font.pixelSize: 24
            text: "Авторизация"
            background: Rectangle {
            implicitWidth: 100
            implicitHeight: 40
            color: "#1262cb"
            border.width: 1
            radius: 5
            }

            onClicked:{
                httpController.logIN(login.text, password.text);
            }
        }
    }

    ScrollView {
        anchors.top: mainLayout.bottom
        anchors.topMargin: 98

        anchors.left: parent.left
        anchors.leftMargin: 15

        anchors.right: parent.right
        anchors.rightMargin: 15

        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15

        clip:  true

        TextArea{
            id: base64data
            textFormat: Text.RichText
            //objectName: "textArea"
            readOnly: true
            anchors.fill: parent
            anchors.topMargin: -8

            color: "#1262cb"

            background: Rectangle {
                id: news
                color: "#ffffff"
                visible: false
            }
        }
    }
}
